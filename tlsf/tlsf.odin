// Based on the Two Level Segregated Fit (TLSF) paper: http://www.gii.upv.es/tlsf/
// Also, heavily inspired by Sebastian Aaltonen's OffsetAllocator: https://github.com/sebbbi/OffsetAllocator

// TODO: maybe switch out the current handles approach with the the one implemented in utils?

package tlsf

Allocator :: ^TLSF

create :: proc(size: u32, max_allocs: u32 = 128 * 1024) -> Allocator {
    tlsf := new(TLSF)
    tlsf^ = TLSF {
        bin_indices = { 0..<BINS_COUNT = UNUSED },
        nodes = make([dynamic]Node, 0, max_allocs),
        unused_node_idxs = make([dynamic]Node_Idx, 0, max_allocs),
        size = size,
        max_allocs = max_allocs,
    }

    insert_node(tlsf, &Node {
        prev_bin_idx = UNUSED,
        next_bin_idx = UNUSED,
        prev_neighbor_idx = UNUSED,
        next_neighbor_idx = UNUSED,
        offset = 0,
        size = size,
        free = true,
    })

    return tlsf
}

destroy :: proc(self: Allocator) {
    delete(self.nodes)
    delete(self.unused_node_idxs)
    free(self)
}

Allocation :: struct {
    offset: u32,
    node_metadata: Node_Idx,
}

allocate :: proc(self: Allocator, requested_size: u32) -> Allocation {
    assert(requested_size >= MIN_ALLOCATABLE_BLOCK_SIZE, "Requested allocation size is too small")

    _, fl, sl := mapping_search(requested_size)
    free_node_idx := find_suitable_node_idx(self^, fl, sl)
    assert(free_node_idx != UNUSED, "Out of memory")

    free_node := self.nodes[free_node_idx]
    assert(free_node.free, "Overwriting active allocation")
    assert(free_node.size >= requested_size, "Insuffecient size")

    remove_node(self, free_node_idx)

    if free_node.size - requested_size >= MIN_ALLOCATABLE_BLOCK_SIZE {
        new_node := Node {
            prev_bin_idx = UNUSED,
            next_bin_idx = UNUSED,
            prev_neighbor_idx = free_node_idx,
            next_neighbor_idx = UNUSED,
            offset = free_node.offset + requested_size,
            size = free_node.size - requested_size,
            free = true,
        }
        inserted_node_idx := insert_node(self, &new_node)
        self.nodes[free_node_idx].next_neighbor_idx = inserted_node_idx
        self.nodes[free_node_idx].size = requested_size
    }

    return Allocation { offset = free_node.offset, node_metadata = free_node_idx }
}

deallocate :: proc(self: Allocator, allocation: Allocation) {
    node := &self.nodes[allocation.node_metadata]
    assert(node.free == false, "Double freeing an allocation")

    merge_neighbors(self, node)
    // Mark the node's index as unused so we can reuse its node index,
    // otherwise a new node will be pushed to the nodes buffer
    append(&self.unused_node_idxs, allocation.node_metadata)
    assert(len(self.unused_node_idxs) <= auto_cast self.max_allocs, "Exceeded maximum number of allocations")
    insert_node(self, node)

    node.free = true
}
