//+private
package tlsf

import "base:intrinsics"

SL_Type :: distinct u8
Node_Idx :: u32
L :: 3 + size_of(SL_Type) - 1
SAUTRATED_U32 :: 0xFFFFFFFF
SATURATED_SL_TYPE :: 0xFF
UNUSED: Node_Idx : auto_cast SAUTRATED_U32
BINS_COUNT :: 32 * 8 * size_of(SL_Type)
MIN_ALLOCATION_EXPONENT :: 5
MIN_ALLOCATABLE_BLOCK_SIZE :: 1 << MIN_ALLOCATION_EXPONENT

Node :: struct {
    prev_bin_idx: Node_Idx,
    next_bin_idx: Node_Idx,
    prev_neighbor_idx: Node_Idx,
    next_neighbor_idx: Node_Idx,
    offset: u32,
    size: u32,
    free: bool, // TODO: Remove later and perhaps use the high bit in one of the vars as a flag?
}

TLSF :: struct {
    nodes: [dynamic]Node,
    unused_node_idxs: [dynamic]Node_Idx,
    fl_bitmap: u32,
    size: u32,
    bin_indices: [BINS_COUNT]Node_Idx,
    sl_bitmaps: [32]SL_Type,
    max_allocs: u32,
}

fls :: proc(num: u32) -> u32 { return num == 0 ? 0 : 31 - intrinsics.count_leading_zeros(num) }
ffs :: proc(num: u32) -> u32 { return num == 0 ? 0 : intrinsics.count_trailing_zeros(num) }

mapping_insert :: proc(r: u32) -> (fl, sl: u32) {
    assert(r >= MIN_ALLOCATABLE_BLOCK_SIZE, "Requested allocation size is too small")

    fl = fls(r)
    sl = (r >> (fl - L)) - (1 << L)
    fl -= MIN_ALLOCATION_EXPONENT

    return fl, sl
}

insert_node :: proc(self: ^TLSF, node: ^Node) -> (inserted_node_idx := UNUSED) {
    assert(node.size >= MIN_ALLOCATABLE_BLOCK_SIZE, "Requested allocation size is too small")

    fl, sl := mapping_insert(node.size)
    bin_idx := (fl << L) | sl
    curr_top := self.bin_indices[bin_idx]

    if len(self.unused_node_idxs) > 0 {
        inserted_node_idx = pop(&self.unused_node_idxs)
        self.nodes[inserted_node_idx] = node^
    } else {
        inserted_node_idx = auto_cast len(self.nodes)
        append(&self.nodes, node^)
        assert(len(self.nodes) <= auto_cast self.max_allocs, "Exceeded maximum number of allocations")
    }

    if curr_top == UNUSED {
        self.bin_indices[bin_idx] = inserted_node_idx
    } else {
        node.next_bin_idx = curr_top
        self.nodes[curr_top].prev_bin_idx = inserted_node_idx
    }

    self.fl_bitmap |= 1 << fl
    self.sl_bitmaps[fl] |= 1 << sl

    return inserted_node_idx
}

remove_node :: proc(self: ^TLSF, node_idx: Node_Idx) {
    // Remove node from available nodes and update accordingly
    node := self.nodes[node_idx]
    self.nodes[node_idx].free = false
    if node.next_bin_idx != UNUSED {
        self.nodes[node.next_bin_idx].prev_bin_idx = node.prev_bin_idx
    }
    if node.prev_bin_idx != UNUSED {
        self.nodes[node.prev_bin_idx].next_bin_idx = node.next_bin_idx
    }

    fl, sl := mapping_insert(node.size)

    bin_idx := fl << L | sl
    if self.bin_indices[bin_idx] == node_idx {
        self.bin_indices[bin_idx] = node.next_bin_idx
    }

    // Update bitmaps if bin is now empty
    if self.bin_indices[bin_idx] == UNUSED {
        self.sl_bitmaps[fl] &= ~(1 << sl)
        if self.sl_bitmaps[fl] == 0 {
            self.fl_bitmap &= ~(1 << fl)
        }
    }
}

mapping_search :: proc(r: u32) -> (r_modified, fl, sl: u32) {
    assert(r >= MIN_ALLOCATABLE_BLOCK_SIZE, "Requested allocation size is too small")

    r_modified = r + (1 << (fls(r) - L)) - 1
    fl = fls(r_modified)
    sl = (r_modified >> (fl - L)) - (1 << L)
    fl -= MIN_ALLOCATION_EXPONENT

    return r_modified, fl, sl
}

find_suitable_node_idx :: proc(self: TLSF, fl, sl: u32) -> Node_Idx {
    non_empty_fl, non_empty_sl: u32
    bitmap_temp := self.sl_bitmaps[fl] & (SAUTRATED_U32 << sl)
    if bitmap_temp != 0 {
        non_empty_fl = fl
        non_empty_sl = ffs(auto_cast bitmap_temp)
    } else {
        bitmap_temp := self.fl_bitmap & (SAUTRATED_U32 << (fl + 1))
        non_empty_fl = ffs(bitmap_temp)
        non_empty_sl = ffs(auto_cast self.sl_bitmaps[non_empty_fl])
    }

    bin_idx := (non_empty_fl << L) | non_empty_sl
    return self.bin_indices[bin_idx]
}

merge_neighbors :: proc(self: ^TLSF, node: ^Node) {
    if node.prev_neighbor_idx != UNUSED && self.nodes[node.prev_neighbor_idx].free {
        prev_node := self.nodes[node.prev_neighbor_idx]

        // Remove neighbor
        remove_node(self, node.prev_neighbor_idx)

        // Mark the neigbor node as unused
        append(&self.unused_node_idxs, node.prev_neighbor_idx)
        assert(len(self.unused_node_idxs) <= auto_cast self.max_allocs, "Exceeded maximum number of allocations")

        // Update node with neighbor's data
        node.prev_neighbor_idx = prev_node.prev_neighbor_idx
        node.offset = prev_node.offset
        node.size += prev_node.size
    }

    if node.next_neighbor_idx != UNUSED && self.nodes[node.next_neighbor_idx].free {
        next_node := self.nodes[node.next_neighbor_idx]

        // Remove neighbor
        remove_node(self, node.next_neighbor_idx)

        // Mark the neigbor node as unused
        append(&self.unused_node_idxs, node.next_neighbor_idx)
        assert(len(self.unused_node_idxs) <= auto_cast self.max_allocs, "Exceeded maximum number of allocations")

        // Update node with neigbor's data
        node.next_neighbor_idx = next_node.next_neighbor_idx
        node.size += next_node.size
    }
}
